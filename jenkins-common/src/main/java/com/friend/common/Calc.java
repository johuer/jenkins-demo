package com.friend.common;

/**
 * <p></p>
 *
 * @author jiuhua.xu
 * @version 1.0
 * @since JDK 1.8
 */

public class Calc {

    public int add(int a, int b) {
        return a + b;
    }

}
