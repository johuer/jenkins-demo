package com.friend.server.controller;

import com.friend.common.Calc;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p></p>
 *
 * @author jiuhua.xu
 * @version 1.0
 * @since JDK 1.8
 */
@RestController
@RequestMapping("test")
public class TestController {

    @GetMapping("first")
    public ResponseEntity first() {
        Calc calc = new Calc();

        return ResponseEntity.ok("Hello Jenkins! Calc -> " + calc.add(1, 2));
    }

}
